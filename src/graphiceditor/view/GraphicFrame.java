package graphiceditor.view;

import graphiceditor.decorator.NumberDecorator;
import graphiceditor.decorator.OutLine;
import graphiceditor.model.GraphicModel;
import graphiceditor.objects.Circle;
import graphiceditor.objects.GraphicObject;
import graphiceditor.visitor.BackwardVisitor;
import graphiceditor.visitor.ForwardVisitor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Frame class for the graphical objects.
 */
public class GraphicFrame extends JFrame {

	private static final long serialVersionUID = -2329212433155597042L;
	private final GraphicModel model;
	private final GraphicView view;
	private GraphicObject selectedPrototype;
	JButton b1, b2;
	private boolean num, select;

	/**
	 * Constructs the frame with the model.
	 *
	 * @param model the graphical model
	 */
	public GraphicFrame(GraphicModel model, GraphicObject... graphicObjects) {
		super("Graphic Editor");
		this.model = model;
		this.num = true;
		this.getContentPane().setLayout(new BorderLayout());
		view = new GraphicView();
		this.getContentPane().add(view, BorderLayout.CENTER);
		view.setBackground(Color.WHITE);

		final JToolBar toolBar = new JToolBar();
		this.getContentPane().add(toolBar, BorderLayout.PAGE_START);

		ButtonGroup buttonGroup = new ButtonGroup();
		for (GraphicObject g : graphicObjects) {
			JToggleButton button = new JToggleButton(g.toString());
			button.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					selectedPrototype = g;
				}
			});
			buttonGroup.add(button);
			toolBar.add(button);
		}

		// I wanted to keep the option tu turn Numbers or Toggle on or off
		b1 = new JButton("Toggle Num");
		toolBar.add(b1);
		b1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				num = !num;
			}
		});
//		b2 = new JButton("Toggle select");
//		toolBar.add(b2);
//		b2.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				select = !select;
//			}
//		});

		// Task 2: Create buttons that are based on a prototype graphic object.
		//         Implement ActionListener to set selected prototype object.


	}

	/**
	 * Generates a random color
	 *
	 * @return a randomly generated color
	 */
	public static Color getRandomColor() {
		int x = (int) (Math.random() * Integer.MAX_VALUE);
		return new Color(x, false);
	}

	/**
	 * The view class for drawing the graphical objects. 
	 */
	@SuppressWarnings("serial")
	private class GraphicView extends JComponent {

		/**
		 * Constructor for the graphical view. 
		 * Adds listener for mouse events and key events and a 
		 * listener to the model for repainting the view upon model changes. 
		 */
		public GraphicView() {
			super();
			setPreferredSize(new Dimension(1024, 768));

			this.addKeyListener(new KeyAdapter() {
				public void keyPressed(java.awt.event.KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
						select = true;
					}
				}

				;

				public void keyReleased(java.awt.event.KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
						select = false;
					}
				}

				;
			});

			this.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					requestFocusInWindow();
					int x = e.getX();
					int y = e.getY();

					//Task 2: Clone prototype to create a new graphicObject

					if (selectedPrototype == null) {
						selectedPrototype = new Circle(0, 0, 10);
					}
					GraphicObject graphicObject = selectedPrototype.copy(x, y);

					if (graphicObject == null) {
						return;
					}


					graphicObject.setColor(getRandomColor());
					if (num) {
						graphicObject = new NumberDecorator(graphicObject);
					}
					if (select) {
						graphicObject = new OutLine(graphicObject);
					}
					model.add(graphicObject);

				}
			});

			this.addKeyListener(new KeyAdapter() {
				public void keyPressed(java.awt.event.KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
						// Task 4: Apply forwardVisitor to model
						final ForwardVisitor forwardVisitor = new ForwardVisitor();
						for (GraphicObject g : model.getGraphicObjects()) {
							g.accept(forwardVisitor);
						}

					} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
						//  Task 4: Apply backwardVisitor to model
						final BackwardVisitor backwardVisitor = new BackwardVisitor();
						for (GraphicObject g : model.getGraphicObjects()) {
							g.accept(backwardVisitor);
						}
					}
				};
			});

			model.addGraphicChangedListener(ce -> {
				repaint();
			});
		}
		
		/**
		 * Paints the graphical object of the model. 
		 * @param g the graphics context
		 */
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);

			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			for (GraphicObject o : model.getGraphicObjects()) {
				g.setColor(o.getColor());
				o.paint(g);
			}
		}

	}

}
