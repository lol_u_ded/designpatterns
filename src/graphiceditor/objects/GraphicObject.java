package graphiceditor.objects;

import graphiceditor.visitor.Visitor;

import java.awt.*;

/**
 * Base class for graphical objects.
 */
public abstract class GraphicObject implements Cloneable {

	protected int x;
	protected int y;
	private Color color;
	private final int ID;

	/**
	 * Constructor initializing x- and y-coordinate
	 *
	 * @param x x-coordinate
	 * @param y y-coordinate
	 */
	protected GraphicObject(int x, int y, int ID) {
		this.x = x;
		this.y = y;
		this.ID = ID;
	}

	/**
	 * Gets the x-coordinate
	 * @return the x-coordinate
	 */
	public int getX() {
		return x;
	}


	/**
	 * Gets the y-coordinate
	 * @return the y-coordinate
	 */
	public int getY() {
		return y;
	}


	/**
	 * Gets the color of this object
	 * @return the color
	 */

	public Color getColor() {
		return color;
	}

	/**
	 * Sets the x-coordinate
	 * @param x the x-coordinate
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Sets the y-coordinate
	 * @param y the y-coordinate
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Sets the color
	 * @param c the color
	 */
	public void setColor(Color c) {
		this.color = c;
	}

	/**
	 * Paints the graphical object on this graphic object
	 * @param g the graphics context 
	 */
	public abstract void paint(Graphics g);

	/**
	 * Gets the width of this object
	 * @return the width
	 */
	public abstract int getWidth();

	/**
	 * Gets the height of this object
	 *
	 * @return the height
	 */
	public abstract int getHeight();

	//  Task 2: copy method
	public GraphicObject copy(int xNew, int yNew) {
		GraphicObject graphicObject;
		try {
			graphicObject = (GraphicObject) super.clone();
			graphicObject.setX(xNew);
			graphicObject.setY(yNew);
			graphicObject.setColor(this.color);
			return graphicObject;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int getID() {
		return ID;
	}

	public abstract void accept(Visitor visitor);

}
