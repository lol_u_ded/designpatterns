package graphiceditor.objects;

import graphiceditor.visitor.Visitor;

import java.awt.*;
import java.util.stream.IntStream;

public class Star extends GraphicObject {

    private final int radius;
    private final int points;
    private final double rotation;
    // ID = 3;

    private Polygon polygon;

    public Star(int x, int y, int radius, int points, double rotation) {
        super(x, y, 3);
        this.radius = radius;
        this.points = points;
        this.rotation = rotation;
        generateStar();
    }

    private void generateStar() {
        final int tips = points * 2;
        final int[] radiusArr = {this.radius, this.radius / 2};
        final double offset = (Math.PI * 2) / tips;
        int[] xArr = new int[tips];
        int[] yArr = new int[tips];


        IntStream.range(0, tips).forEach(i -> {
            xArr[i] = (int) (Math.cos((i * offset) + rotation) * radiusArr[i % 2] + x);
            yArr[i] = (int) (Math.sin((i * offset) + rotation) * radiusArr[i % 2] + y);
        });
        this.polygon = new Polygon(xArr, yArr, tips);
    }

    @Override
    public void paint(Graphics g) {

        g.setColor(getColor());
        g.fillPolygon(polygon);
    }

    @Override
    public int getWidth() {
        return polygon.getBounds().width;
    }

    @Override
    public int getHeight() {
        return polygon.getBounds().height;
    }

    @Override
    public GraphicObject copy(int xNew, int yNew) {
        return new Star(xNew, yNew, radius, points, rotation);
    }

    @Override
    public String toString() {
        return "Star[" +
                "r= " + radius +
                ", p= " + points +
                ']';
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
