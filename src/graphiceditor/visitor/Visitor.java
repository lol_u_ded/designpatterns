package graphiceditor.visitor;

import graphiceditor.model.GraphicModel;
import graphiceditor.objects.Circle;
import graphiceditor.objects.GraphicObject;
import graphiceditor.objects.Rectangle;
import graphiceditor.objects.Star;

public interface Visitor {

    void visit(Rectangle rectangle);

    void visit(Star star);

    void visit(Circle circle);

    void visit(GraphicModel graphicModel);

    void visit(GraphicObject next);
}
