package graphiceditor.visitor;

import graphiceditor.model.GraphicModel;
import graphiceditor.objects.Circle;
import graphiceditor.objects.GraphicObject;
import graphiceditor.objects.Rectangle;
import graphiceditor.objects.Star;

import java.awt.*;


public class ForwardVisitor implements Visitor {
    @Override
    public void visit(Rectangle rectangle) {
        rectangle.setY(rectangle.getY() - 4);

    }

    @Override
    public void visit(Star star) {
        star.setColor(Color.BLACK);
    }

    @Override
    public void visit(Circle circle) {
        circle.setX(circle.getX() + 4);
    }

    @Override
    public void visit(GraphicModel graphicModel) {
        //todo
    }

    @Override
    public void visit(GraphicObject next) {
        if (next.getID() == 1) {
            visit((Circle) next);
        } else if (next.getID() == 2) {
            visit((Rectangle) next);
        } else if (next.getID() == 3) {
            visit((Star) next);
        }
    }
}
