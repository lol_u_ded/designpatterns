package graphiceditor;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import graphiceditor.model.GraphicModel;
import graphiceditor.objects.Circle;
import graphiceditor.objects.Rectangle;
import graphiceditor.objects.Star;
import graphiceditor.view.GraphicFrame;

/**
 * Main class for starting the application. 
 */
public class Main {

	public static void main(final String[] args) throws IOException {

		final GraphicModel model = new GraphicModel();
		final Rectangle rectangle = new Rectangle(0, 0, 50, 50);
		final Circle circle = new Circle(0, 0, 50);
		final Star star = new Star(500, 92, 50, 5, 5.0);
		// create prototypes, factories etc. needed to initialize application
		final JFrame frame = new GraphicFrame(model, star, circle, rectangle);
		SwingUtilities.invokeLater(() -> {
			frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			frame.setVisible(true);
			frame.pack();
		});
	}
}
