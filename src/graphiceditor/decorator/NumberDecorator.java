package graphiceditor.decorator;

import graphiceditor.objects.GraphicObject;
import graphiceditor.visitor.Visitor;

import java.awt.*;

public class NumberDecorator extends Decorator {

    public static int count = 1;
    private final int curr;

    public NumberDecorator(GraphicObject next) {
        super(next);
        this.curr = count;
        count++;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.WHITE);
        g.drawString(Integer.toString(curr), getNext().getX() - 2, getNext().getY() + 4);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(getNext());
    }
}
