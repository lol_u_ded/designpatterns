package graphiceditor.decorator;

import graphiceditor.objects.GraphicObject;
import graphiceditor.visitor.Visitor;

import java.awt.*;

public class OutLine extends Decorator {

    public OutLine(GraphicObject next) {
        super(next);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.BLACK);
        g.drawRect(getNext().getX() - getNext().getWidth() / 2,
                getNext().getY() - getNext().getHeight() / 2,
                getNext().getWidth(),
                getNext().getHeight());
    }

    @Override
    public void accept(Visitor visitor) {
        // no Movement
    }
}
