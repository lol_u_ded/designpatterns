package graphiceditor.decorator;

import graphiceditor.objects.GraphicObject;
import graphiceditor.visitor.Visitor;

import java.awt.*;

public abstract class Decorator extends GraphicObject {

    private final GraphicObject next;

    protected Decorator(GraphicObject next) {
        super(next.getX(), next.getY(), next.getID());
        this.next = next;
    }

    public GraphicObject getNext() {
        return next;
    }

    @Override
    public int getX() {
        return next.getX();
    }

    @Override
    public int getY() {
        return next.getY();
    }

    @Override
    public Color getColor() {
        return next.getColor();
    }

    @Override
    public void setColor(Color c) {
        next.setColor(c);
    }

    @Override
    public void paint(Graphics g) {
        next.paint(g);
    }

    @Override
    public int getWidth() {
        return next.getWidth();
    }

    @Override
    public int getHeight() {
        return next.getHeight();
    }

    @Override
    public GraphicObject copy(int xNew, int yNew) {
        return next.copy(xNew, yNew);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(next);
    }
}
